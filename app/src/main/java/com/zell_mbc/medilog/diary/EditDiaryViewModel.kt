package com.zell_mbc.medilog.diary

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.EditDataViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class EditDiaryViewModel(application: Application): EditDataViewModel(application) {
    override lateinit var repository: DiaryRepository
    lateinit var dao: DiaryDao

    fun init() {
        dao = MediLogDB.getDatabase(getApplication()).diaryDao()
        repository = DiaryRepository(dao, 0L, 0L)
    }

    override fun getItem(id: Int): Diary? {
        var item: Diary? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun update(item: Diary) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(item)
    }
}