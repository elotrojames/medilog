package com.zell_mbc.medilog.base

import com.zell_mbc.medilog.DataItem

abstract class DataRepository {
    var filterStart = 0L
    var filterEnd = 0L

    abstract suspend fun delete(id: Int)
    abstract suspend fun delete(tmpComment: String) // Required to identify temporary items in EditFragments
    abstract suspend fun deleteAll()
    abstract suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long)

    abstract fun getSize(filterStart: Long, filterEnd: Long): Int
    abstract fun getItem(index: Int): DataItem
    abstract fun getItems(order: String, filterStart: Long, filterEnd: Long): List<DataItem>
}