package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import java.text.*
import java.util.*
import kotlin.math.max
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class WaterChartFragment : Fragment() {
    private var waterThreshold = ArrayList<Int>()
    private var waters = ArrayList<Int>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.water_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", "Empty Context")
            return
        }

        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0
        var wMin = 10000

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(c)

//        val daySteppingMode = sharedPref.getBoolean(SettingsActivity.KEY_PREF_waterDayStepping, false)
        val barChart = sharedPref.getBoolean(SettingsActivity.KEY_PREF_waterBarChart, true)
        var sTmp: String

        var fTmp: Int
        val simpleDate = SimpleDateFormat("MM-dd")
//        val lastDate = Calendar.getInstance()
//        var currentDate: Date?

        val viewModel = ViewModelProvider(this).get(WaterViewModel::class.java)
        viewModel.init()
        val items = viewModel.getDays(true)
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            // Chart stepping by day
 /*           if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                /*currentDate.
                Log.d("--------------- Debug", "currentDate: $currentDate")
                Log.d("--------------- Debug", "lastDate: " + lastDate.time)
                Log.d("--------------- Debug", "Compare: " + currentDate.compareTo(lastDate.time)) */
                if (currentDate != null) {
                    while (labels.size > 0 && currentDate > lastDate.time) {
                        sTmp = simpleDate.format(lastDate.time)
                        labels.add(sTmp)
                        Log.d("--------------- Debug", "GapDate: $sTmp")
                        waters.add(0)
                        lastDate.add(Calendar.DAY_OF_MONTH, 1)
                    }
                }
                if (currentDate != null) lastDate.time = currentDate
            }*/
            labels.add(sTmp)
            waters.add(wi.water)

            // Keep min and max values
            fTmp = wi.water
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (waters.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val thresholdValue = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_waterThreshold, "2000")!!)
        val threshold = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWaterThreshold, true)
        if (threshold) {
            for (item in waters) {
                waterThreshold.add(thresholdValue)
            }
        }


        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.waterPlot)

        val showGrid = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWaterGrid, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = Paint(Color.GRAY)
            plot.graph.rangeGridLinePaint = Paint(Color.GRAY) // Horizontal lines
        }

        // Check text size and apply a factor to y-axis position
        // 42 for standard text size
        // 48 for large
        // 55 for largest
        val size = TextView(requireContext()).textSize
        var leftPadding = 0f
        if (size >= 48 ) leftPadding = 10f
        if (size >= 55 ) leftPadding = 25f
        plot.graph.paddingLeft = leftPadding

        val isLegendVisible = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWaterLegend, true)
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        val series1: XYSeries = SimpleXYSeries(waters, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.water))
        val series2: XYSeries = SimpleXYSeries(waterThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        val wMinBoundary = 0 // wMin - 10
        val wMaxBoundary = max(wMax, thresholdValue)  + 100
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary / 10 * 10) // Set to a 10er value

        if (series1.size() < 10) plot.setDomainStep(StepMode.SUBDIVIDE, series1.size().toDouble()) // Avoid showing the same day multiple times

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 500.0)
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + waterUnit));  // Set integer y-Axis label

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        if (barChart) {
            val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
            plot.addSeries(series1, series1Format)
        } else {
            val series1Format = LineAndPointFormatter(Color.BLUE, null, null, null)
            plot.addSeries(series1, series1Format)
        }

        if (threshold) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 1f
            plot.addSeries(series2, formatThreshold)
        }
/*
        // Trendlines
        val waterLinarTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WATER_LINEAR_TRENDLINE, false)
        val waterMovingAverageTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WATER_MOVING_AVERAGE_TRENDLINE, false)

        if (waterLinarTrendLine) {
            calculateLinearTrendLine()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            val trendLine: XYSeries = SimpleXYSeries(linearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Linear Trend")

            trendFormat.isLegendIconEnabled = false
            plot.addSeries(trendLine, trendFormat)
        }

        if (waterMovingAverageTrendLine) {
            period = sharedPref.getString(SettingsActivity.KEY_PREF_WATER_MOVING_AVERAGE_SIZE, "5")!!.toInt()
            calculateMovingAverage()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            trendFormat.isLegendIconEnabled = false
            val simpleMovingAverage: XYSeries = SimpleXYSeries(movingAverage, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Moving Average")
            plot.addSeries(simpleMovingAverage, trendFormat)
        }
*/
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}