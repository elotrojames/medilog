package com.zell_mbc.medilog.bloodpressure

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R

//import com.takisoft.preferencex.PreferenceFragmentCompat

class BloodPressureSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.bloodpressurepreferences, rootKey)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}