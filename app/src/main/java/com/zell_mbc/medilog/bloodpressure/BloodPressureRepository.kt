package com.zell_mbc.medilog.bloodpressure

import android.util.Log
import androidx.lifecycle.LiveData
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.DataRepository

class BloodPressureRepository(private val dao: BloodPressureDao, fS:Long, fE: Long): DataRepository() {

    init {
        filterStart = fS
        filterEnd = fE
    }

    fun getItems(filterStart: Long, filterEnd: Long): LiveData<List<BloodPressure>> {
        Log.d("DiaryRepository ", "getItems: $filterStart, $filterEnd")
        return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getItemsGreaterThan(filterStart)
        else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getItemsLessThan(filterEnd)
        else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getItemsRange(filterStart, filterEnd)
        else dao.getItems()
    }

    override fun getItems(order: String, filterStart: Long, filterEnd: Long): List<BloodPressure> {
        if (order == "ASC") {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getItemsASCGreaterThan(filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getItemsASCLessThan(filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getItemsASCRange(filterStart, filterEnd)
            return dao.getItemsASC()
        } else {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getItemsDESCGreaterThan(filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getItemsDESCLessThan(filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getItemsDESCRange(filterStart, filterEnd)
            return dao.getItemsDESC()
        }
    }

    override fun getSize(filterStart: Long, filterEnd: Long): Int {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getSizeGreaterThan(filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getSizeLessThan(filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getSizeRange(filterStart, filterEnd)
        return dao.getSize()
    }

    override fun getItem(index: Int): BloodPressure = dao.getItem(index)
    fun getItem(commentValue: String): BloodPressure = dao.getItem(commentValue)

    fun getMaxSys(): Int = dao.getMaxSys()
    fun getMinSys(): Int = dao.getMinSys()
    fun getMaxDia(): Int = dao.getMaxDia()
    fun getMinDia(): Int = dao.getMinDia()

    suspend fun insert(item: BloodPressure): Long { return dao.insert(item) }
    suspend fun update(item: BloodPressure) { dao.update(item) }
    suspend fun delete(item: BloodPressure) { dao.delete(item) }
    override suspend fun delete(tmpComment: String) { dao.delete(tmpComment) }

    override suspend fun delete(id: Int) { dao.delete(id) }
    override suspend fun deleteAll() { dao.deleteAll() }
    override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) { dao.deleteAllFiltered(filterStart, filterEnd) }
}