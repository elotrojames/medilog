package com.zell_mbc.medilog.bloodpressure

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.EditDataViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class EditBloodPressureViewModel(application: Application): EditDataViewModel(application) {
    override lateinit var repository: BloodPressureRepository
    lateinit var dao: BloodPressureDao

    fun init() {
        dao = MediLogDB.getDatabase(getApplication()).bloodPressureDao()
        repository = BloodPressureRepository(dao, 0L, 0L)
    }

    override fun getItem(id: Int): BloodPressure? {
        var item: BloodPressure? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun update(item: BloodPressure) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(item)
    }
}