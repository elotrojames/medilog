package com.zell_mbc.medilog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.afollestad.date.dayOfMonth
import com.afollestad.date.month
import com.afollestad.date.year
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.datePicker
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.base.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.filter.*
import java.text.DateFormat
import java.util.*


class FilterFragment: DialogFragment() {
    var filterStart = 0L
    var filterEnd = 0L
    private var minStartCal: Calendar = Calendar.getInstance()
    private var maxEndCal: Calendar = Calendar.getInstance()
    private var filterStartCal: Calendar = Calendar.getInstance()
    private var filterEndCal: Calendar = Calendar.getInstance()  // Sets end and start dates to today

    lateinit var viewModel: DataViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filter, container, false)
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var activeTab = ""
        if (arguments != null) activeTab = arguments?.getString("activeTab").toString()
        if (activeTab == "") return super.onViewCreated(view, savedInstanceState)

        when (activeTab) {
            getString(R.string.tab_weight) -> viewModel = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)
            getString(R.string.tab_bloodpressure) -> viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
            getString(R.string.tab_diary) -> viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
            getString(R.string.tab_water) -> viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java)
        }

        filterStart = viewModel.filterStart
        filterEnd = viewModel.filterEnd

        tvHeader.text = getString(R.string.setFilter)

        if (filterEnd != 0L) filterEndCal.timeInMillis = filterEnd

        // Set button text to date if a filter is set
        if (filterStart != 0L) btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStart)

        // Set button text to date if a filter is set
        if (filterEnd != 0L) btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEnd) // Set to today

        // Min date value
        val items = viewModel.getItems("ASC", filtered = false)
        if (items.count() > 0) minStartCal.timeInMillis = items[0].timestamp
        else  {
            minStartCal.timeInMillis = 0
            Snackbar.make(requireView(), getString(R.string.emptyTable), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }
        Log.d("Start: ", " " + minStartCal.dayOfMonth + "." + minStartCal.month + "." + minStartCal.year )

        //##############
        btCancel.setOnClickListener {
            dismiss()
        }

        btSave.setOnClickListener {
            viewModel.setFilter(filterStart, filterEnd) // Make sure Values are updated
            requireActivity().invalidateOptionsMenu()  // Make sure the filter icon is up to date

            dismiss()
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        btStartDate.setOnClickListener {
            MaterialDialog(requireContext()).show {
//                Log.d("Start: ", " " + minStartCal.dayOfMonth + "." + minStartCal.month + "." + minStartCal.year )
//                Log.d("Start: ", " " + filterStartCal.dayOfMonth + "." + filterStartCal.month + "." + filterStartCal.year )
                datePicker(null,maxEndCal, minStartCal) { _, date->
                    if (date < filterEndCal) {
                        filterStart = date.timeInMillis
                        filterStartCal = date
                        setButtonText(false)
                    } else {
                        Snackbar.make(requireView(), getString(R.string.invalidFilterStartDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                        filterStartCal.timeInMillis = viewModel.getItems("ASC", filtered = false)[0].timestamp // Reset to timestamp of first DB entry
                    }
                }
            }
        }

        btEndDate.setOnClickListener {
            MaterialDialog(requireContext()).show {
                datePicker(null, maxEndCal, null) { _, date->
                    if (date > maxEndCal) {
                        filterEndCal = date
                        filterEnd = filterEndCal.timeInMillis
                        setButtonText(true)
                    } else {
                        Snackbar.make(requireView(), getString(R.string.invalidFilterEndDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    }
                }
            }
        }

        btDeleteFilterStart.setOnClickListener {
            btStartDate.text = getString(R.string.startDate)
            filterStart = 0
            filterStartCal.timeInMillis = 0
        }


        btDeleteFilterEnd.setOnClickListener {
            btEndDate.text = getString(R.string.endDate)
            filterEndCal.timeInMillis = Calendar.getInstance().timeInMillis
            filterEnd = 0
        }

    }

    private fun setButtonText(end: Boolean) {
        if (end) btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEndCal.time)
        else btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStartCal.time)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}