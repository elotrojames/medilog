package com.zell_mbc.medilog

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat

class TabsSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.tabspreferences, rootKey)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}