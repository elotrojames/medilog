## v1.8.1.1, build 5313
New 
- Nothing

Fixed
- Fixed a bug which accepted various delimiters during import file header heck check, but expected the set one during the actual import.
- De-branded screenshots
- Introduced 4th digit in version number to properly reflect bug fix releases for F-Droid
- Added entries for missing French translation items to make F-Droid compile

Known issues
- Setting/Changing the filter is not reflected in the data tabs right away. Needs a manual screen refresh via launching About or Settings screen or, of course restarting the app
