App Internal - Significant refactoring 
- Switched to Google recommended architecture
- Utilizing Room, Data Migrations, Repositories, ViewModels and LiveData
- Migrated from Java to Kotlin
- Switched from fingerprint to Biometric libraries 
- Upgraded to AndroidX

New/improved
- Protect app with biometric (fingerprint) logon
- Cleaner PDF reports, no more hard to see colours but bold and underline for elevated values
- New default color theme blue, added ability to switch colour themes
- Added MovingAverage Trendlines
- Added proper edit dialogs / fragments
- Switch chart grid on/off 
- Show weight  and blood pressure thresholds in chart

Fixes
- Fixed bug when highlighting elevated values, #2
- Fixed bug where deleted values remained in chart, #49
- Fixed a bug in BloodPressure PDF report. Diastolic value didn't show 
